(defpackage clip/tests/main
  (:use :cl
        :clip
        :rove))
(in-package :clip/tests/main)

;; NOTE: To run this test file, execute `(asdf:test-system :clip)' in your Lisp.

(deftest test-target-1
  (testing "should (= 1 1) to be true"
    (ok (= 1 1))))
