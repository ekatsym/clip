(defsystem "clip"
  :version "0.1.0"
  :author "Hirotetsu Hongo"
  :license "LLGPL"
  :components ((:module "src"
                :serial t
                :components
                ((:file "package")
                 (:file "util")
                 (:file "core")
                 (:module "io"
                  :serial t
                  :components
                  ((:file "util")
                   (:file "tiff")
                   (:file "main"))))))
  :description ""
  :in-order-to ((test-op (test-op "clip/tests"))))

(defsystem "clip/tests"
  :author "Hirotetsu Hongo"
  :license "LLGPL"
  :depends-on ("clip"
               "rove")
  :components ((:module "tests"
                :components
                ((:file "main"))))
  :description "Test system for clip"
  :perform (test-op (op c) (symbol-call :rove :run c)))
