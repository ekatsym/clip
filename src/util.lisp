(defpackage clip.util
  (:use :common-lisp)
  (:export #:index
           #:enumerate #:consn #:force-list
           #:take #:drop
           #:insert
           #:append1
           #:mappend
           #:group

           #:partial #:rpartial #:nth-partial

           #:read-byte-array
           #:read-file-into-byte-array

           #:in

           #:map-array #:do-array #:convolute-array

           #:aif #:it
           #:alambda #:self

           #:list-monad
           ))
(in-package clip.util)

;;; declation
(declaim (inline drop force-list append1))

;;; list
(deftype index ()
  '(and (integer 0 *) fixnum))

(define-condition index-error (error)
  ((datum :initarg :datum)
   (index :initarg :index))
  (:report
    (lambda (o s)
      (with-slots (datum index) o
        (format s "Invalid index ~a in ~a." index datum)))))

(defun enumerate (count &key (start 0) (step 1))
  (let ((x start)
        (acc '()))
    (dotimes (i count (nreverse acc))
      (psetq x (+ x step)
             acc (cons x acc)))))

(defun consn (n se1 se2)
  (labels ((rec (i acc)
             (declare (optimize (speed 3))
                      (type index i))
             (if (<= i 0)
                 acc
                 (rec (1- i) (cons se1 acc)))))
    (rec n se2)))

(defun force-list (x)
  (if (atom x) (list x) x))

(defun take (lst n)
  (labels ((rec (n lst acc)
             (declare (optimize (speed 3))
                      (type index n))
             (if (<= n 0)
                 (nreverse acc)
                 (rec (1- n) (rest lst) (cons (first lst) acc)))))
    (rec n lst '())))

(defun drop (lst n)
  (nthcdr n lst))

(defun insert (x lst index)
  (declare (type index index))
  (labels ((rec (i l acc)
             (declare (optimize (speed 3))
                      (type index i))
             (cond ((zerop i)
                    (revappend acc (cons x l)))
                   ((endp l)
                    (error 'index-error :datum lst :index index))
                   (t
                    (rec (1- i) (rest l) (cons (first l) acc))))))
    (rec index lst '())))

(defun append1 (lst x)
  (append lst (list x)))

(defun mappend (fn lst1 &rest more-lsts)
  (declare (type function fn))
  (let ((lsts (cons lst1 more-lsts)))
    (labels ((rec (lsts acc)
               (declare (optimize (speed 3)))
               (if (some #'endp lsts)
                   (nreverse acc)
                   (rec (mapcar #'rest lsts) (revappend (apply fn (mapcar #'first lsts)) acc)))))
      (rec lsts '()))))

(defun group (lst n)
  (labels ((rec (l acc)
             (declare (optimize (speed 3)))
             (if (null l)
                 (nreverse acc)
                 (rec (drop l n) (cons (take l n) acc)))))
    (rec lst '())))

;;; function
(defun partial (fn &rest args)
  (lambda (&rest rest-args) (apply fn (append args rest-args))))

(defun rpartial (fn &rest args)
  (lambda (&rest rest-args) (apply fn (append rest-args args))))

(defun nth-partial (fn n x)
  (lambda (&rest args) (apply fn (insert x args n))))

;;; io
(defun read-byte-array (stream)
  (let* ((len (file-length stream))
         (vec (make-array len :element-type '(unsigned-byte 8))))
    (dotimes (i len vec)
      (setf (aref vec i) (read-byte stream)))))

(defun read-file-into-byte-array (filespec)
  (with-open-file (s filespec :element-type '(unsigned-byte 8))
    (read-byte-array s)))

;;; macro
(defmacro in (x &rest choices)
  (let ((var (gensym "X")))
    `(let ((,var ,x))
       (or ,@(mapcar (lambda (choice) `(eql ,var ,choice))
                     choices)))))

;;; array
(defun map-2darray (function 2darray &rest more-2darrays)
  (let* ((dimensions (array-dimensions 2darray))
         (element-type (array-element-type 2darray))
         (result (make-array dimensions :element-type element-type)))
    (dotimes (i (first dimensions))
      (dotimes (j (second dimensions))
        (setf (aref result i j)
              (apply function
                     (aref 2darray i j)
                     (mapcar (lambda (a) (aref a i j))
                             more-2darrays)))))
    result))

(defun map-3darray (function 3darray &rest more-3darrays)
  (let* ((dimensions (array-dimensions 3darray))
         (element-type (array-element-type 3darray))
         (result (make-array dimensions :element-type element-type)))
    (dotimes (i (first dimensions))
      (dotimes (j (second dimensions))
        (dotimes (k (third dimensions))
          (setf (aref result i j k)
                (apply function
                       (aref 3darray i j k)
                       (mapcar (lambda (a) (aref a i j k))
                               more-3darrays))))))
    result))

(defun map-array (function array &rest more-arrays)
  (let* ((rank (array-rank array))
         (dimensions (array-dimensions array))
         (element-type (array-element-type array))
         (result (make-array dimensions :element-type element-type)))
    (cond ((= rank 2)
           (apply #'map-2darray function array more-arrays))
          ((= rank 3)
           (apply #'map-3darray function array more-arrays))
          (t
           (labels ((next-pos (pos last-pos acc)
                      (declare (optimize (speed 3)))
                      (cond ((and (endp pos) (endp last-pos))
                             'end)
                            ((= (the index (first pos)) (the index (first last-pos)))
                             (next-pos (rest pos) (rest last-pos) (cons (first pos) acc)))
                            ((< (the index (first pos)) (the index (first last-pos)))
                             (consn (list-length acc)
                                    0
                                    (cons (1+ (the index (first pos)))
                                          (rest pos)))))))
             (let ((last-pos (mapcar #'1- dimensions)))
               (do ((pos (make-list rank :initial-element 0)
                         (next-pos pos last-pos '())))
                   ((eq pos 'end) result)
                   (setf (apply #'aref result pos)
                         (apply function
                                (apply #'aref array pos)
                                (mapcar (lambda (a) (apply #'aref a pos))
                                        more-arrays))))))))))

(defmacro do-array ((var array &optional (result nil)) &body body)
  (let ((g!array (gensym "ARRAY"))
        (g!rank (gensym "RANK"))
        (g!dimensions (gensym "DIMENSIONS"))
        (g!end (gensym "END"))
        (g!pos (gensym "POS"))
        (g!last-pos (gensym "LAST-POS"))
        (g!next-pos (gensym "NEXT-POS"))
        (g!acc (gensym "ACC")))
    `(let* ((,g!array ,array)
            (,g!rank (array-rank ,g!array))
            (,g!dimensions (array-dimensions ,g!array))
            (,g!end (gensym "END")))
       (labels ((,g!next-pos (,g!pos ,g!last-pos ,g!acc)
                  (declare (optimize (speed 3)))
                  (cond ((and (endp ,g!pos) (endp ,g!last-pos))
                         ,g!end)
                        ((= (the index (first ,g!pos)) (the index (first ,g!last-pos)))
                         (,g!next-pos (rest ,g!pos) (rest ,g!last-pos) (cons (first ,g!pos) ,g!acc)))
                        ((< (the index (first ,g!pos)) (the index (first ,g!last-pos)))
                         (consn (list-length ,g!acc)
                                0
                                (cons (1+ (the index (first ,g!pos)))
                                      (rest ,g!pos)))))))
         (let ((,g!last-pos (mapcar #'1- ,g!dimensions)))
           (do ((,g!pos (make-list ,g!rank :initial-element 0)
                        (,g!next-pos ,g!pos ,g!last-pos '())))
               ((eq ,g!pos ,g!end) ,result)
               (let ((,var (apply #'aref ,g!array ,g!pos)))
                 ,@body)))))))

(defun convolute-2darray (2darray filter)
  (let* ((dimensions (array-dimensions 2darray))
         (filter-dimensions (array-dimensions filter))
         (shift-dimensions (mapcar (rpartial #'floor 2) filter-dimensions))
         (result (make-array dimensions)))
    (dotimes (i (first dimensions))
      (dotimes (j (second dimensions))
        (let ((x 0))
          (dotimes (k (first filter-dimensions))
            (dotimes (l (second filter-dimensions))
              (let ((k+ (+ k (- i (first shift-dimensions))))
                    (l+ (+ l (- j (second shift-dimensions)))))
                (incf x (* (if (and (<= 0 k+ (1- (first dimensions)))
                                    (<= 0 l+ (1- (second dimensions))))
                               (aref 2darray k+ l+)
                               (aref 2darray i j))
                           (aref filter k l))))))
          (setf (aref result i j) x))))
    result))

(defun convolute-array (array filter)
  (let* ((rank (array-rank array))
         (dimensions (array-dimensions array))
         (filter-dimensions (array-dimensions filter))
         (shift-dimensions (mapcar (rpartial #'floor 2) filter-dimensions))
         (result (make-array dimensions)))
    (cond ((= rank 2) (convolute-2darray array filter))
          (t
           (labels ((next-pos (pos last-pos acc)
                      (cond ((and (endp pos) (endp last-pos))
                             'end)
                            ((= (first pos) (first last-pos))
                             (next-pos (rest pos) (rest last-pos) (cons (first pos) acc)))
                            ((< (first pos) (first last-pos))
                             (consn (list-length acc)
                                    0
                                    (cons (1+ (the index (first pos)))
                                          (rest pos)))))))
             (let ((last-pos (mapcar #'1- dimensions))
                   (last-fpos (mapcar #'1- filter-dimensions)))
               (do ((pos (make-list rank :initial-element 0)
                         (next-pos pos last-pos '())))
                   ((eq pos 'end) result)
                   (let ((x 0))
                     (do ((fpos (make-list rank :initial-element 0)
                                (next-pos fpos last-fpos '())))
                         ((eq fpos 'end))
                         (let ((fpos+ (mapcar (lambda (p fp sd) (+ fp (- p sd)))
                                              pos fpos shift-dimensions)))
                           (incf x (* (if (every (lambda (fp+ d) (<= 0 fp+ (1- d)))
                                                 fpos+
                                                 dimensions)
                                          (apply #'aref array fpos+)
                                          (apply #'aref array pos))
                                      (apply #'aref filter fpos)))))
                     (setf (apply #'aref result pos) x)))
               result))))))


;;; anaphora
(defmacro aif (test then &optional else)
  `(let ((it ,test))
     (if it ,then ,else)))

(defmacro alambda (args &body body)
  `(labels ((self ,args ,@body))
     #'self))

;;; monad
(defmacro list-monad (bindings &body body)
  (let ((result (gensym "RESULT")))
    (labels ((rec (bindings body)
               (if (endp bindings)
                   `(push (progn ,@body) ,result)
                   (destructuring-bind ((var lst) &rest rest-bindings) bindings
                     `(dolist (,var ,lst)
                        ,(rec rest-bindings body))))))
      `(let ((,result '()))
         ,(rec bindings body)
         (nreverse ,result)))))
