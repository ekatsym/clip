(defpackage clip.io.tiff
  (:use :cl)
  (:import-from :clip.util
                #:index
                #:enumerate #:force-list
                #:append1
                #:mappend
                #:group
                #:rpartial
                #:in
                #:aif #:it)
  (:import-from :clip.core
                #:make-image
                #:image-width
                #:image-height
                #:image-channels
                #:image-size)
  (:import-from :clip.io.util
                #:parse-byte
                #:parse-bytes
                #:unparse-bytes)
  (:export #:parse-tiff))
(in-package :clip.io.tiff)

;;; tables
(defparameter *tag-table*
  '(;; essential
    (256 . image-width)
    (257 . image-length)
    (258 . bits/sample)
    (259 . compression)
    (262 . photometric-interpretation)
    (273 . strip-offsets)
    (278 . rows/strip)
    (279 . strip-byte-counts)
    (282 . x-resolution)
    (283 . y-resolution)
    (296 . resolution-unit)
    (320 . color-map)

    ;; selective
    (254 . new-subfile-type)
    (263 . threhsolding)
    (264 . cell-width)
    (265 . cell-length)
    (266 . fill-order)
    (270 . image-description)
    (271 . make)
    (272 . model)
    (274 . orientation)
    (277 . samples/pixel)
    (280 . min-sample-value)
    (281 . max-sample-value)
    (284 . planar-configuration)
    (290 . gray-response-unit)
    (291 . gray-response-curve)
    (305 . software)
    (306 . date-time)
    (315 . artist)
    (316 . host-computer)
    (338 . extra-samples)
    (33432 . copyright)

    ;; extention
    (330 . sub-ifds)
    ))

(defun tag-name (tag-code)
  (aif (assoc tag-code *tag-table*)
       (cdr it)
       'xtag))

(defun tag-code (tag-name)
  (aif (rassoc tag-name *tag-table*)
       (car it)
       0))

(defparameter *data-type-table*
  '((1 . byte)
    (2 . ascii)
    (3 . short)
    (4 . long)
    (5 . rational)
    (6 . sbyte)
    (7 . undefined)
    (8 . sshort)
    (9 . slong)
    (10 . srational)
    (11 . float)
    (12 . double)))

(defun type-name (type-code)
  (aif (assoc type-code *data-type-table*)
       (cdr it)
       'xtype))

(defun type-code (type-name)
  (aif (rassoc type-name *data-type-table*)
       (car it)
       0))

(defparameter *compression-table*
  '((1 . uncompressed)
    (5 . lzw)
    (6 . jpeg)
    (7 . jpeg2)
    (8 . zip)
    (32773 . packbits)))

(defun compression-name (compression-code)
  (aif (assoc compression-code *compression-table*)
       (car it)
       'xcompression))

(defun compression-code (compression-name)
  (aif (rassoc compression-name *tag-table*)
       (cdr it)
       0))

(defparameter *photometric-interpretation-table*
  '((0 . white-is-zero)
    (1 . black-is-zero)
    (2 . rgb)
    (3 . palette-color)
    (4 . transparency-mask)))

;;; util
(defun byte-order->from-end (byte-order)
  (ecase byte-order
    (77 nil)
    (73 t)))

(defun from-end->byte-order (from-end)
  (ecase from-end
    ((nil) 77)
    ((t) 73)))

(defun type-size (type-code)
  (ecase type-code
    ((1 2 6 7) 1)
    ((3 8) 2)
    ((4 9 11) 4)
    ((5 10 12) 8)
    ((13) 12)))

;;; core
(defstruct tiff-image
  byte-array byte-order ifd-count ifds)

(defstruct ifd
  ;; essential
  image-width image-length bits/sample compression
  photometric-interpretation strip-offsets rows/strip strip-byte-counts
  x-resolution y-resolution resolution-unit color-map

  ;; selective
  new-subfile-type thresholding cell-width cell-length
  fill-order image-description make model orientation
  samples/pixel min-sample-value max-sample-value
  planar-configuration gray-response-unit gray-response-curve
  software date-time artist host-computer extra-samples copyright

  ;; extended
  sub-ifds
  (xtag '()))

(defstruct ifd-entry
  tag type count data)

;;; byte-array -> tiff-image
(defun parse-tiff (byte-array)
  (let ((from-end (if (= (parse-byte byte-array 0)
                         (parse-byte byte-array 1))
                      (byte-order->from-end (parse-byte byte-array 0))
                      (error "unknown byte order"))))
    (assert (= (parse-bytes byte-array 2 2 from-end) 42) () "not TIFF")
    (labels ((ifd-iter (offset acc)
               (declare (optimize (speed 3))
                        (type index offset))
               (if (zerop offset)
                   (make-tiff-image :byte-array byte-array
                                    :byte-order (if from-end 73 77)
                                    :ifd-count (length acc)
                                    :ifds (nreverse acc))
                   (multiple-value-bind (ifd next-ifd-offset) (parse-ifd byte-array offset from-end)
                     (ifd-iter next-ifd-offset (cons ifd acc))))))
      (ifd-iter (parse-bytes byte-array 4 4 from-end) '()))))

(defun parse-ifd (byte-array offset from-end)
  (let ((count (parse-bytes byte-array offset 2 from-end))
        (ifd (make-ifd)))
    (dotimes (i count)
      (let* ((current-offset (+ offset 2 (* i 12)))
             (tag (parse-bytes byte-array current-offset 2 from-end))
             (tag-name (tag-name tag))
             (type (parse-bytes byte-array (+ current-offset 2)2 from-end))
             (count (parse-bytes byte-array (+ current-offset 4) 4 from-end))
             (type-size (type-size type))
             (data-size (* type-size count))
             (data (if (= count 1)
                       (if (in data-size 1 2 3 4)
                           (parse-bytes byte-array
                                        (+ current-offset 8)
                                        data-size
                                        from-end)
                           (parse-bytes byte-array
                                        (parse-bytes byte-array
                                                     (+ current-offset 8)
                                                     4
                                                     from-end)
                                        data-size
                                        from-end))
                       (if (in data-size 1 2 3 4)
                           (loop :for j :below count
                                 :collect (parse-bytes byte-array
                                                       (+ current-offset 8 (* j type-size))
                                                       type-size
                                                       from-end))
                           (let ((data-offset (parse-bytes byte-array (+ current-offset 8) 4 from-end)))
                             (loop :for j :below count
                                   :collect (parse-bytes byte-array
                                                         (+ data-offset (* j type-size))
                                                         type-size
                                                         from-end)))))))
        (if (eq tag-name 'xtag)
            (push (make-ifd-entry
                    :tag tag
                    :type type
                    :count count
                    :data data)
                  (slot-value ifd 'xtag))
            (setf (slot-value ifd tag-name)
                  (make-ifd-entry
                    :tag tag
                    :type type
                    :count count
                    :data data)))))
    (values ifd (parse-bytes byte-array (+ offset 2 (* 12 count)) 4 from-end))))

;;; tiff-image -> images
(defun tiff-image->images (tiff-image)
  (with-slots (byte-array byte-order ifds) tiff-image
    (mapcar (rpartial #'ifd->image byte-array (byte-order->from-end byte-order))
            ifds)))

(defun ifd->image (ifd byte-array from-end)
  (with-slots (image-width image-length bits/sample strip-offsets strip-byte-counts) ifd
    (let* ((height (ifd-entry-data image-length))
           (width (ifd-entry-data image-width))
           (channels (ifd-entry-count bits/sample))
           (depth-bytes (mapcar (rpartial #'/ 8) (force-list (ifd-entry-data bits/sample))))
           (offsets (force-list (ifd-entry-data strip-offsets)))
           (byte-counts (force-list (ifd-entry-data strip-byte-counts))))
      (make-image-from-strips height width channels
                              byte-array offsets depth-bytes byte-counts from-end))))

(defun make-image-from-strips (height width channels byte-array offsets depth-bytes byte-counts from-end)
  (declare (type index height width channels)
           #+sbcl (sb-ext:muffle-conditions sb-ext:compiler-note))
  (let ((image (make-image height width channels)))
    (labels ((rec1 (h w c offsets counts)
               (declare (optimize (speed 3)))
               (if (endp offsets)
                   (if (endp counts)
                       image
                       (error "Different count between strip-offsets and byte-counts."))
                   (if (endp counts)
                       (error "Different count between strip-offsets and byte-counts.")
                       (multiple-value-bind (h w c) (rec2 h w c
                                                          (first offsets)
                                                          (first depth-bytes)
                                                          (first counts))
                         (rec1 h w c (rest offsets) (rest counts))))))
             (rec2 (h w c offset size count)
               (declare (optimize (speed 3))
                        (type index h w c offset size count))
               (cond ((zerop count)
                      (values h w c))
                     ((plusp count)
                      (setf (aref image h w c)
                            (parse-bytes byte-array offset size from-end))
                      (if (= c (1- channels))
                          (if (= w (1- width))
                              (rec2 (1+ h) 0 0
                                    (+ offset size)
                                    (next-size size)
                                    (- count size))
                              (rec2 h (1+ w) 0
                                    (+ offset size)
                                    (next-size size)
                                    (- count size)))
                          (rec2 h w (1+ c)
                                (+ offset size)
                                (next-size size)
                                (- count size))))
                     (t
                      (error "Do not match bits-per-sample and byte-counts."))))
             (next-size (size)
               (or (second (member size depth-bytes)) (first depth-bytes))))
      (rec1 0 0 0 offsets byte-counts))))

;;; images -> tiff-image
(defun images->tiff-image (images depth)
  (labels ((images->ifds (imgs offset acc)
             (declare (optimize (speed 3)))
             (if (endp imgs)
                 (nreverse acc)
                 (multiple-value-bind (ifd next-offset) (image->ifd+offset (first imgs) offset depth)
                   (images->ifds (rest imgs) next-offset (cons ifd acc))))))
    (let ((ifds (images->ifds images 8 '())))
      (make-tiff-image
        :byte-array '()
        :byte-order 77
        :ifd-count (length ifds)
        :ifds ifds))))

(defun image->ifd+offset (image offset depth)
  (let* ((ifd-size (* 12 12))
         (strip-max-size (expt 2 13))
         (strip-counts (ceiling (image-size image) strip-max-size))
         (channels (image-channels image)))
    (let ((bits/sample-bytes (if (= channels 1) 0 (* channels 2)))
          (strip-offsets-bytes (* strip-counts 4))
          (strip-byte-counts-bytes (* strip-counts 4))
          (x-resolution-bytes 8)
          (y-resolution-bytes 8)
          (color-map-bytes (* channels 2)))
      (let* ((ifd-entry-offset (+ offset 2))
             (ifd-pointer-offset (+ ifd-entry-offset ifd-size))
             (bits/sample-offset (+ ifd-pointer-offset 4))
             (strip-offsets-offset (+ bits/sample-offset bits/sample-bytes))
             (strip-byte-counts-offset (+ strip-offsets-offset strip-offsets-bytes))
             (x-resolution-offset (+ strip-byte-counts-offset strip-byte-counts-bytes))
             (y-resolution-offset (+ x-resolution-offset x-resolution-bytes))
             (color-map-offset (+ y-resolution-offset y-resolution-bytes))
             (first-strip-offset (+ color-map-offset color-map-bytes))
             (strip-offsets (enumerate strip-counts :start first-strip-offset :step strip-max-size)))
        (make-ifd :image-width
                  (make-ifd-entry
                    :tag (tag-code 'image-width)
                    :type (if (< (image-width image) (expt 256 2))
                              (type-code 'short)
                              (type-code 'long))
                    :count 1
                    :data (image-width image))

                  :image-length
                  (make-ifd-entry
                    :tag (tag-code 'image-length)
                    :type (if (< (image-height image) (expt 256 2))
                              (type-code 'short)
                              (type-code 'long))
                    :count 1
                    :data (image-height image))

                  :bits/sample
                  (make-ifd-entry
                    :tag (tag-code 'bits/sample)
                    :type (type-code 'short)
                    :count (image-channels image)
                    :data (if (= channels 1)
                              depth
                              bits/sample-offset))

                  :compression
                  (make-ifd-entry
                    :tag (tag-code 'compression)
                    :type (type-code 'short)
                    :count 1
                    :data 1)

                  :photometric-interpretation
                  (make-ifd-entry
                    :tag (tag-code 'photometric-interpretation)
                    :type (type-code 'short)
                    :count 1
                    :data (ecase channels
                            ((1) 1)
                            ((3) 2)))

                  :strip-offsets
                  (make-ifd-entry
                    :tag (tag-code 'strip-offsets)
                    :type (type-code 'long)
                    :count strip-counts
                    :data (if (= strip-counts 1)
                              (first strip-offsets)
                              strip-offsets))

                  :rows/strip
                  (make-ifd-entry
                    :tag (tag-code 'rows/strip)
                    :type (type-code 'long)
                    :count 1
                    :data (/ strip-max-size (image-width image)))

                  :strip-byte-counts
                  (make-ifd-entry
                    :tag (tag-code 'strip-byte-counts)
                    :type (type-code 'long)
                    :count strip-counts
                    :data (if (= strip-counts 1)
                              (mod (image-size image) strip-max-size)
                              (append1 (make-list (1- strip-counts) :initial-element strip-max-size)
                                       (mod (image-size image) strip-max-size))))

                  :x-resolution
                  (make-ifd-entry
                    :tag (tag-code 'x-resolution)
                    :type (type-code 'rational)
                    :count 1
                    :data 4294967368)

                  :y-resolution
                  (make-ifd-entry
                    :tag (tag-code 'x-resolution)
                    :type (type-code 'rational)
                    :count 1
                    :data 4294967368)

                  :resolution-unit
                  (make-ifd-entry
                    :tag (tag-code 'resolution-unit)
                    :type (type-code 'short)
                    :count 1
                    :data 2)

                  :color-map
                  (make-ifd-entry
                    :tag (tag-code 'color-map)
                    :type (type-code 'short)
                    :count 1
                    :data 0))))))

#|
(defun make-byte-array (images ifds)
  (labels ((rec (imgs ifds acc)
             (declare (optimize (speed 3)))
             (if (or (endp imgs) (endp ifds))
                 (coerce (list* 77 77 0 42 0 0 0 8 (nreverse acc)) 'simple-vector)
                 (rec (rest imgs)
                      (rest ifds)
                      (revappend (make-sub-byte-list (first img) (first ifd)) acc))))
           (make-bytes (n size)
             (do ((i size (1- i))
                  (n n (mod n (expt 256 i)))
                  (acc '() (cons (floor (expt 256 i)) acc)))
                 ((< i 0) (nreverse acc))))
           (make-sub-byte-list (img ifd)
             (mappend (lambda (ifd-entry)
                        (and ifd-entry
                             (with-slots (tag type count data) ifd-entry
                               (append (unparse-bytes tag 2)
                                       (unparse-bytes type 2)
                                       (unparse-bytes count 4)
                                       (unparse-bytes data 4))))
                        )
                      (with-slots (;; essential tag
                                   image-width image-length bits/sample compression
                                   photometric-interpretation strip-offsets rows/strip
                                   strip-byte-counts x-resolution y-resolution
                                   resolution-unit color-map

                                   ;; selective tag
                                   new-subfile-type thresholding cell-width cell-length
                                   fill-order image-description make model orientation
                                   samples/pixel min-sample-value max-sample-value
                                   planar-configuration gray-response-unit gray-response-curve
                                   software date-time artist host-computer extra-samples
                                   copyright sub-ifds xtag) ifd
                        (list image-width image-length bits/sample compression
                              photometric-interpretation strip-offsets rows/strip
                              strip-byte-counts x-resolution y-resolution
                              resolution-unit color-map
                              new-subfile-type thresholding cell-width cell-length
                              fill-order image-description make model orientation
                              samples/pixel min-sample-value max-sample-value
                              planar-configuration gray-response-unit gray-response-curve
                              software date-time artist host-computer extra-samples
                              copyright sub-ifds xtag)
                        ))
             )
           )
    )
  )

(defun image->strips (image)
  (let ((height (image-height image))
        (width (image-width image))
        (channels (image-channels image)))
    (declare (type index height width channels))
    (labels ((rec (h w c n strip strips)
               (declare (optimize (speed 3))
                        (type index h w c n)
                        #+sbcl (sb-ext:muffle-conditions sb-ext:compiler-note))
               (if (<= c 0)
                   (if (<= w 0)
                       (if (<= h 0)
                           (cons (cons (aref image h w c) strip) strips)
                           (if (<= n 0)
                               (rec (1- h) (1- width) (1- channels) (1- (expt 2 13))
                                    '() (cons (cons (aref image h w c) strip) strips))
                               (rec (1- h) (1- width) (1- channels) (1- n)
                                    (cons (aref image h w c) strip) strips)))
                       (if (<= n 0)
                           (rec h (1- w) (1- channels) (1- (expt 2 13))
                                '() (cons (cons (aref image h w c) strip) strips))
                           (rec h (1- w) (1- channels) (1- n)
                                (cons (aref image h w c) strip) strips)))
                   (if (<= n 0)
                       (rec h w (1- c) (1- (expt 2 13))
                            '() (cons (cons (aref image h w c) strip) strips))
                       (rec h w (1- c) (1- n)
                            (cons (aref image h w c) strip) strips)))))
      (rec (1- height) (1- width) (1- channels) (1- (expt 2 13)) '() '()))))


(let ((byte-array (clip.util:read-file-into-byte-array "img4.tif")))
  (setq *print-length* 100)
  (time (parse-tiff byte-array)))
(let ((byte-array (clip.util:read-file-into-byte-array "img.tif")))
  (setq *print-length* 30)
  (time (tiff-image->images (parse-tiff byte-array))))
(let ((byte-array (clip.util:read-file-into-byte-array "img2.tif")))
  (setq *print-length* 30)
  (time (tiff-image->images (parse-tiff byte-array))))
(let ((byte-array (clip.util:read-file-into-byte-array "img3.tif")))
  (setq *print-length* 100)
  (image->raw-strips (print (car (tiff-image->images (print (parse-tiff byte-array)))))))
(let ((byte-array (clip.util:read-file-into-byte-array "img.tif")))
  (setq *print-length* 100)
  (time (images->tiff-image (tiff-image->images (parse-tiff byte-array)) 8)))
|#
