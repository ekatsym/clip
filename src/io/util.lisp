(defpackage clip.io.util
  (:use :common-lisp)
  (:import-from :clip.util
                #:index)
  (:export #:parse-byte
           #:parse-bytes
           #:unparse-bytes
           )
  )
(in-package :clip.io.util)

(declaim (inline parse-byte))

(defun parse-byte (byte-array offset)
  (aref byte-array offset))

(defun parse-bytes (byte-array offset size from-end)
  (declare (type index offset size)
           (type (simple-array (unsigned-byte 8) (*)) byte-array))
  (labels ((rec (i acc)
             (declare (optimize (speed 3))
                      (type (and fixnum (integer -1 *)) i)
                      (type integer acc)
                      #+sbcl(sb-ext:muffle-conditions sb-ext:compiler-note))
             (if (< i 0)
                 acc
                 (rec (1- i)
                      (+ acc
                         (the integer
                              (* (the (unsigned-byte 8)
                                      (parse-byte byte-array
                                                  (the index
                                                       (+ offset i))))
                                 (the integer
                                      (expt 256 (the index
                                                     (if from-end
                                                         i
                                                         (- size i 1))))))))))))
    (rec (1- size) 0)))

(defun unparse-bytes (val size)
  (do ((i size (1- i))
       (n val (mod val (expt 256 i)))
       (acc '() (cons (floor val (expt 256 i)) acc)))
      ((< i 0) (nreverse acc))))

(define-condition invalid-image-file-format (error)
  ((file :initarg :file :reader invalid-image-file-format))
  (:report
    (lambda (o s)
      (format s "A format of the file~%~2t~a~%is invalid for TIFF."
              (invalid-image-file-format o)))))
