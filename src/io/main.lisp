(defpackage clip.io
  (:use :common-lisp)
  (:import-from :clip
                #:read-image
                #:write-image))
(in-package :clip.io)
