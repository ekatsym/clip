(defpackage clip
  (:export
    ;; core
    #:image
    #:imagep
    #:image-height
    #:image-width
    #:image-channels
    #:image-size
    #:make-image

    ;; io
    #:read-image
    #:write-image
    ))
