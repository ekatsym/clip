(defpackage clip.core
  (:use :cl)
  (:import-from :clip
                #:image
                #:imagep
                #:image-height
                #:image-width
                #:image-channels
                #:image-size
                #:make-image))
(in-package :clip.core)

;;; declaim
(declaim (inline imagep image-height image-width image-channels make-image))

;;; image
(deftype image (&optional height width channels)
  `(array unsigned-byte (,height ,width ,channels)))

(defun imagep (object)
  (typep object 'image))

(defun image-height (image)
  (array-dimension image 0))

(defun image-width (image)
  (array-dimension image 1))

(defun image-channels (image)
  (array-dimension image 2))

(defun image-size (image)
  (array-total-size image))

(defun make-image (height width channels &key  (initial-element 0))
  (make-array (list height width channels)
              :element-type 'unsigned-byte
              :initial-element initial-element))
